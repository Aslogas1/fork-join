import java.io.IOException;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ForkJoinPool;

public class Main {
    public static void main(String[] args) {
        MapParser parser = new MapParser();
        Set<String> compute = null;
        try {
            compute = parser.getChildren("https://skillbox.ru/");
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
        parser.writeInFile(Objects.requireNonNull(compute));
        compute.forEach(System.out::println);
        ForkJoinPool forkJoinPool = new ForkJoinPool();
        forkJoinPool.invoke(parser);
        compute.forEach(System.out::println);
    }
}
