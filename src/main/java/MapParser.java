import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Set;
import java.util.concurrent.RecursiveTask;
import java.util.stream.Collectors;

public class MapParser extends RecursiveTask<Set<String >> {

    private final String ATTR = "abs:href";

    public void writeInFile(Set<String> set) {
        try {
            FileWriter writer = new FileWriter("src/main/resources/links.txt");
            String URL = "https://skillbox.ru/";
            writer.write(URL + System.lineSeparator());
            for (String s : set) {
                if(s.split("/").length == 3){
                    writer.write("\t" + s + System.lineSeparator());
                } else if(s.split("/").length == 4) {
                    writer.write("\t\t" + s + System.lineSeparator());
                }else if(s.split("/").length == 5) {
                    writer.write("\t\t\t" + s + System.lineSeparator());
                }else {
                    writer.write("\t\t\t\t" + s + System.lineSeparator());
                }
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Set<String> getChildren(String link) throws InterruptedException, IOException {
            Document doc = Jsoup.connect(link).ignoreHttpErrors(true).maxBodySize(0).get();
            return doc.select("a").stream()
                    .parallel()
                    .map(el -> el.attr(ATTR))
                    .filter(el -> !el.contains("#") && el.endsWith("/"))
                    .collect(Collectors.toSet());

    }

    @Override
    protected Set<String> compute() {
        Set<String> set = null;
        try {
            set = getChildren("https://skillbox.ru/");
            Thread.sleep(125);
            fork();
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
        return set;
    }
}